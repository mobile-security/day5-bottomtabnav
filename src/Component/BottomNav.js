import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../App/Home';
import History from '../App/History';
import Login from '../App/Login';

const Tab = createBottomTabNavigator();
const BottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        // tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarActiveTintColor: '#009966',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://img.icons8.com/clouds/512/home-page.png',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarActiveTintColor: '#009966',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://img.icons8.com/clouds/512/time-machine.png',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Logout"
        component={Login}
        options={{
          tabBarActiveTintColor: '#009966',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://img.icons8.com/clouds/512/logout-rounded.png',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNav;
