import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import React from 'react';
import icBack from '../icons/back.png';

const GrabFood = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View style={styles.box}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                onPress={() => {
                  // navigation.navigate('HomeScreen');
                  navigation.goBack();
                  // goBack() adalah sebuah function dari si navigation di mana dia simpan navigasi history dan kembali ke screen 1
                }}>
                <View>
                  <Image
                    source={{
                      uri: 'https://img.icons8.com/carbon-copy/512/circled-left-2.png',
                    }}
                    style={{
                      resizeMode: 'contain',
                      width: 50,
                      height: 50,
                      bottom: 10,
                    }}></Image>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              height: Platform.OS == 'android' ? 250 : 280,
              width: Platform.OS == 'android' ? 390 : 370,

              backgroundColor: '#white',

              borderRadius: 10,
              margin: 10,
              justifyContent: 'center',
              alignContent: 'center',
            }}>
            <Image
              source={{
                uri: 'https://www.ojolakademi.com/wp-content/uploads/2022/03/GrabFood-Error.jpg',
              }}
              style={{
                height: 240,
                top: 150,
                resizeMode: 'stretch',
              }}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#00B242',
    // alignContent: 'center',
    // justifyContent:'center',
    // alignItems:'center'
  },
  box: {
    width: 0.1,
    height: 50,
    backgroundColor: 'white',
    top: 50,
    bottom: 50,
    marginBottom: 45,
  },
});

export default GrabFood;
