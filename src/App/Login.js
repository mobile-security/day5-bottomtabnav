import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Button,
  StyleSheet,
  SafeAreaView,
  Image,
  Alert,
} from 'react-native';

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = () => {
    // Kode untuk melakukan login
    if (email === 'Admin' && password === 'admin') {
      Alert.alert('Login Berhasil', 'Selamat datang!');
    } else {
      Alert.alert('Login Gagal', 'Email atau password salah.');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.innerContainer}>
        <View>
          <Image
            source={{
              uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Grab_Logo.svg/2560px-Grab_Logo.svg.png',
            }}
            style={{
              height: 100,
              width: 280,
              top: 1,
            }}></Image>
        </View>
        <TextInput
          style={styles.input}
          placeholder="Email"
          keyboardType="email-address"
          value={email}
          onChangeText={text => setEmail(text)}
          style={{
            borderWidth: 1, // ketebalan border
            borderColor: 'grey',
            borderRadius: 15, // curve border, bergantung dgn height
            width: 230,
            height: 40,
            alignSelf: 'center',
            textAlign: 'center',
            top: 10,
          }}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          value={password}
          onChangeText={text => setPassword(text)}
          style={{
            marginTop: 15,
            borderColor: 'grey',
            borderWidth: 1, // ketebalan border
            borderRadius: 15, // curve border, bergantung dgn height
            width: 230,
            height: 40,
            alignSelf: 'center',
            textAlign: 'center',
            top: 10,
          }}
        />
        <View
          style={{
            marginTop: 10,
            top: 10,
          }}>
          <Button color="#00B242" title="Login" onPress={handleLogin} />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  innerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    width: '100%',
    height: 40,
    marginVertical: 10,
    borderWidth: 1,
    padding: 10,
  },
});

export default Login;
