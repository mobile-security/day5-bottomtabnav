import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';
import GrabFood from './GrabFood';
// import fonts from './assets/fonts';

const History = ({navigation}) => {
  const [item, setItem] = useState(null);

  const dataHistory = async () => {
    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    var raw = '';

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      requestOptions,
    )
      .then(response => response.text())
      .then(result => {
        const val = JSON.parse(result);
        setItem(Object.entries(val));
        console.log(result);
      })
      .catch(error => console.log('error', error));
  };
  useEffect(() => {
    dataHistory();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.box}>
        <View>
          <TouchableOpacity
            onPress={() => {
              // navigation.navigate('HomeScreen');
              navigation.goBack();
              // goBack() adalah sebuah function dari si navigation di mana dia simpan navigasi history dan kembali ke screen 1
            }}>
            <View>
              <Image
                source={{
                  uri: 'https://img.icons8.com/carbon-copy/512/circled-left-2.png',
                }}
                style={{
                  resizeMode: 'contain',
                  width: 50,
                  height: 50,
                  bottom: 10,
                }}></Image>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          backgroundColor: 'white',
        }}>
        <View style={styles.header}>
          <Text style={styles.headerText}>History Pembelian</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.content}>
            {item?.map((e, index) => {
              return (
                <View
                  key={index}
                  style={{
                    borderWidth: 1,
                    borderColor: Platform.OS == 'ios' ? 'white' : null,
                    borderRadius: 10,
                    justifyContent: 'center',
                    margin: 5, //jarak antar kotak enter dgn input
                    padding: 4, //tinggi pendek kotak enter
                    width: 350, //panjang pendek kotak enter
                    backgroundColor: 'white', //background tulisan enter
                    // color:'white', //warna tulisan enter
                    overflow: 'hidden', //penyesuaian borderRadius ios
                    alignSelf: 'center',
                    borderBottomWidth: 3,
                    borderRightWidth: 2,
                    shadowColor: 'grey',
                    shadowOffset: {width: 1, height: 5},
                    shadowOpacity: 0.8,
                    shadowRadius: 3,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View>
                      <Text
                        style={{
                          // fontStyle:'italic',
                          // fontWeight:'bold',
                          // fontFamily: 'TiltPrism-Regular',
                          fontSize: 16,
                        }}>
                        Sender : {e[0]}
                      </Text>
                      <Text
                        style={{
                          // fontFamily: 'Mynerve-Regular',
                          fontSize: 16,
                        }}>
                        Receiver : {e[1].target}
                      </Text>
                      <Text
                        style={
                          {
                            // fontFamily: 'Roboto-Bold', fontSize:14,
                          }
                        }>
                        Status : {e[1].type}
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          // fontFamily: 'Glossy Sheen Shadow DEMO',
                          // fontSize:14,
                        }}>
                        Rp.{e[1].amount}
                      </Text>
                    </View>
                    <Image
                      style={{
                        width: 40,
                        height: 40,
                        resizeMode: 'contain',
                      }}
                      source={{
                        uri: 'https://cdn-icons-png.flaticon.com/512/3734/3734681.png',
                      }}
                    />
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#00B242',
  },
  box: {
    width: 0.1,
    height: 50,
    backgroundColor: 'white',
    top: 50,
    bottom: 50,
    marginBottom: 45,
  },
  header: {
    alignSelf: 'center',
    width: 350,
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-start',
    // backgroundColor: 'yellow',
    // borderRadius: 10,
    // borderBottomWidth: 3,
    // borderRightWidth: 2,
    // shadowColor: 'grey',
    // shadowOffset: {width: 1, height: 5},
    // shadowOpacity: 0.8,
    // shadowRadius: 3,
    // elevation: 3,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  content: {
    color: 'black',
    padding: 10,
    shadowColor: '#171717',
    shadowOffset: {width: 2, height: 8},
    shadowOpacity: 0.8,
    shadowRadius: 3,
    elevation: 3,
  },
});

export default History;
