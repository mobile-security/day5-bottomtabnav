import { View, Text } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import Route from './src/route/Route'


const App = () => {
  return (
    <NavigationContainer>
      <Route/>
    </NavigationContainer>
    )
}

export default App